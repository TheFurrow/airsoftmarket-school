///////////////////////////////////
// THIS VERSION OF AIRSOFT MARKET
// IS ONLY FOR PRESENTATION OF
// SCHOOLWORK AND DOES NOT INCLUDE
// MAIN FUNCTIONS OF REAL SERVER

var express = require('express'),
    session = require('express-session'),
    app = express(),
    ejs = require('ejs'),
    bodyParser = require('body-parser');

var conf = require('./js/server/config.js');

//We want to use EmbeddedJS to run as our view engine
app.set('view engine', 'ejs');
//Now we use / as direction
app.use("/", express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

//Routes are implemented here
var userRoute = require('./js/server/routes/routes_user.js'),
    messagesRoute = require('./js/server/routes/routes_messages.js');

//Webcontent
app.use('/user', userRoute,messagesRoute)

app.listen('3000', function() {
  console.log('Server running on localhost:3000');
});

module.exports.app = app;
