Copyright Tuisku Saarelainen <saarelainen.tuisku@gmail.com>
All rights reserved

* DISTRIBUTION -
You ARE NOT GRANTED to distribute original and
                  or modified source code in any form.
* MODIFICATION -
You ARE GRANTED to modify this source code.
* PATENT GRANT -
You ARE NOT GRANTED to create, request
                  buy, sell patent for original and or modified
                  source code in any form.
* PRIVATE USE -
You ARE GRANTED to use original and or modified
                  source code in private.
* SUBLICENSING - 
You ARE NOT GRANTED to sublicense original
                  and or modified source code in any form.
* TRADEMARK GRANT -
You ARE NOT GRANTED to create and or use TRADEMARK
                  with original and or modified source code in any form.

Written in Joensuu, Finland
November 3th, 2016 
By Tuisku Saarelainen <saarelainen.tuisku@gmail.com>
