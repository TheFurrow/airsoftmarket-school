//Our files are stored here
var ImageArray = [];
window.ImageArray = ImageArray;
var removeArray = [];
window.removeArray = removeArray;

var max_size = 2560000;
var max_images = 7;
var loadedImgs = 0;

function loadedImgsFun() {
  $('.imageInfoRow').empty();
  $('.imageInfoRow').append('Kuvia ladattu: '+ loadedImgs +'/'+max_images);
}
window.loadedImgsFun = loadedImgsFun;

$(document).ready(function() {

  //In the beginning after document is ready,
  //we want to know how many images are there
  //already
  $('.loadedImgs').each(function(index) {
    loadedImgs++;
    loadedImgsFun();
  });

  //Make a list of removable images from remote
  $(document).on('click', '.loadedImgs', function() {
    var image = $(this).attr('alt');

    if($(this).css(["opacity"]).opacity == "0.3") {
      removeArray.push({name:image})
      //$(this).remove();
      console.log('REMOVE THESE',removeArray);

      loadedImgs--;

    }
    else {
      for(var i = 0; i < removeArray.length; i++) {
        if(removeArray[i].name == $(this).attr('alt')) {
          removeArray.splice(i,1);
        }
      }
      console.log('UNREMOVED',removeArray);
      loadedImgs++;
    }

    loadedImgsFun();
  });

  //Make a list of local Images
  $(document).on('click', '.localImage', function() {
    $(this).remove();

    for(var i = 0; i < ImageArray.length; i++) {
      if(ImageArray[i].name == $(this).attr('alt')) {
        ImageArray.splice(i,1);
      }
    }

    console.log("24",ImageArray);
    loadedImgs--;
    loadedImgsFun();
  });


  console.log(loadedImgs);


  //1. Get file(s) from input (files only, no camera for now)
  $(document).on('change', '#input_file_pc', function() {

    //For temporary images
    var IgnoredFilesArray = [];

    //Get files from input
    var files = $(this)[0].files;

    //Save images in loop
    $.each(files, function(index) {


      if(files[index].size < max_size && loadedImgs < max_images) {
        //Save files in array
        ImageArray.push(files[index]);
        loadedImgs++;
      }
      else if(files[index].size > max_size && loadedImgs < max_images){
        var tmp = {
          name: files[index].name,
          size: files[index].size
        }
        IgnoredFilesArray.push(tmp);
      }
      else {
        alert('Voit ladata maksimissaan 7 kuvaa.');
      }

      console.log(files[index]);
    });

    //3. Empty input
    $(this).val('');

    //console.log($(this).val());
    console.log(ImageArray);
    console.log(IgnoredFilesArray);

    //4. Display files from array (image only)
    $('.imageRow').empty();
    $.each(ImageArray, function(index) {
      //console.log('IMAGE ARRAY:',ImageArray[index]);
      var imageCol = '<div class="col-xs-3 col-sm-3 ">'
      imageCol += '<img class="imagePreview img-prev img-prev-del" alt="'+ImageArray[index].name+'" src="'+window.URL.createObjectURL(ImageArray[index])+'"class="img-rounded img-responsive localImage">';
      imageCol += '</div>'
      $('.imageRow').append(imageCol)
      //$('.imageRow').append(index+1 + '. ' +ImageArray[index].name+'<br>');
    });

    //Print out error message
    if(IgnoredFilesArray.length > 0) {
      var message = '<p class="bg-danger">Kuvakoko ylittyi! Kokeile pienentää kuviasi!';
      var tmp = '';
      $.each(IgnoredFilesArray, function(index) {
        if(index != 0 && index < IgnoredFilesArray.length) {
          tmp += ', ';
        }
        tmp += IgnoredFilesArray[index].name;

      });
      message += '<br>'+tmp;
      message += '</p>';
      $('.imageInfoRow').append(message);
    }

    //Update info
    loadedImgsFun();

  });

});
