'use strict';

class Product {
  constructor(category) {
    this.category = category;
    console.log(this.category);
    this.type = {
      Exchange : 0,
      Common : 1,
      Undefined : 2
    }
    this.itemStage = this.type.Undefined;
    if(this.category != 'trade') {
        this.itemStage = this.type.Common;
    }
    else this.itemStage = this.type.Exchange;

    console.log(this.itemStage);
  }

  //Choose right form and return it
  itemTemplate(itemIndex) {

    switch(this.itemStage) {
      case this.type.Exchange : {

        if($('div[data-exchange-row]').length > 1) {
          var item = '<div class="row exchangeRow" data-exchange-row="'+itemIndex+'">'+
                    '<div class="col-xs-12 col-sm-12">'+
                      '<h4>Vaihtotuote '+itemIndex+'</h4>'+
                      '<div class="form-group">'+
                        '<label for="dealItem" class="col-xs-3 control-label">Tuote</label>'+
                        '<div  class="col-xs-9">'+
                          '<input data-id="'+itemIndex+'-name" id="dealItem" type="text" class="form-control">'+
                        '</div>'+

                      '</div>'+
                      '<div class="form-group">'+
                        '<label for="dealItem" class="col-xs-3 control-label">Seloste</label>'+
                        '<div  class="col-xs-9">'+
                          '<textarea data-id="'+itemIndex+'-desc" id="dealItem" type="text" class="form-control"></textarea>'+
                        '</div>'+
                      '</div>'+
                      '<div id="itemsGroup" class="form-group">'+
                        '<div class="col-xs-2">'+
                          '<button id="btn_removeItem" data-id="'+itemIndex+'" type="button" class="btn btn-warning">Poista tuote</button>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '</div>';
          //We will return this for new()
          return item;
        }
        break;
      }
      case this.type.Common : {
        console.log('CHOSE COMMON');
        if($('div[data-product-row]').length > 0) {
          var item = '<div class="row itemsRow" data-product-row="'+itemIndex+'">'+
                                        '<div class="col-centered col-xs-12">'+
                                        '<h4>Tuote '+(itemIndex+1)+'</h4>'+
                                        '<div id="itemsGroup" class="form-group">'+
                                          '<label for="dealItem" class="col-xs-3 control-label">'+'Tuote'+'</label>'+
                                          '<div id="dealItem" class="col-xs-5">'+
                                            '<input id="itemName" data-id="'+itemIndex+'-name" type="text" class="form-control" placeholder="Myytävä tuote">'+
                                          '</div>'+
                                          '<div id="dealItemPrice" class="col-xs-4">'+
                                            '<div class="input-group">'+
                                              '<span class="input-group-addon">'+'€'+'</span>'+
                                              '<input id="itemPrice" data-id="'+itemIndex+'-price" type="text" class="form-control" placeholder="hinta">'+
                                            '</div>'+
                                          '</div>'+
                                        '</div>'+
                                        '<div id="itemsGroup" class="form-group">'+
                                          '<label for="dealItemDescription" class="col-xs-3 control-label">'+'Tuotekuvaus'+'</label>'+
                                          '<div id="dealItemDescription" class="col-xs-9">'+
                                            '<textarea id="itemDescription" data-id="'+itemIndex+'-desc" class="form-control" placeholder=""></textarea>'+
                                          '</div>'+
                                        '</div>'+
                                        '<div id="itemsGroup" class="form-group">'+
                                          '<div class="col-xs-2">'+
                                            '<button id="btn_removeItem" data-id="'+itemIndex+'" type="button" class="btn btn-warning">Poista tuote</button>'+
                                          '</div>'+
                                        '</div>'+
                                        '</div>'+
                                      '</div>';
          //We will return this for new()
          return item;
        }
        break;
      }
    }
  }

  newItem() {
    switch(this.itemStage) {
      case this.type.Exchange: {
        var itemIndex = $('div[data-exchange-row]').length;
        console.log(itemIndex);
        $('#itemContainer div:eq(0)').append(this.itemTemplate(itemIndex));
        break;
      }
      case this.type.Common: {
        var itemIndex = $('div[data-product-row]').length;
        console.log(itemIndex);
        $('#itemContainer div:eq(0)').append(this.itemTemplate(itemIndex));
        break;
      }
    }
  }

  subcategoryTemplate(subcategory) {
    switch (subcategory) {
      case 'weapons': {
        console.log('subcategory:: WEAPONS');
        var template = '<option value="aeg">Sähköaseet (AEG)</option>'+
                        '<option value="gas">Kaasuaseet (Gas)</option>'+
                        '<option value="spring">Jousitoimiset (Spring)</option>'+
                        '<option value="hpa">Paineilma (HPA)</option>'+
                        '<option value="projects">Projektit ja raadot</option>'+
                        '<option value="misc">Sekalainen (kaikenlaista)</option>';
        return template;
        break;
      }
      case 'gear': {
        console.log('subcategory:: GEAR');
        var template = '<option value="combat-gear">Taisteluvarusteet</option>'+
                        '<option value="clothes">Vaatteet</option>'+
                        '<option value="protective">Suojavarusteet</option>'+
                        '<option value="misc">Sekalainen (kaikenlaista)</option>';

        return template;
        break;
      }
      case 'parts': {
        console.log('subcategory:: PARTS');
        var template = '<option value="tuning">Viritysosat</option>'+
                        '<option value="body-parts">Aseiden ulko-osat</option>'+
                        '<option value="mags">Lippaat</option>'+
                        '<option value="misc">Sekalainen (kaikenlaista)</option>';
        return template;
        break;
      }

    }
  }

  newSubCategory(subcategory) {

    if($('#select_dealCategory').val() != 'misc') {
      console.log($('#select_dealCategory').val());
      if($('.form-subcategory-group')) $('.form-subcategory-group').empty();

        var template = '<div class="form-group form-subcategory-group">'+
                            '<label for="selectSubcategory" class="col-xs-3 control-label">'+'Alakategoria'+'</label>'+
                            '<div class="col-xs-9" id="selectCategory">'+
                              '<select id="select_dealSubCategory" class="form-control">';

            template += this.subcategoryTemplate(subcategory);

            template +=     '</select>'+
                            '</div>'+
                          '</div>';

            $('.form-select-group').after(template);
    }
    else $('.form-subcategory-group').empty();

  }
}

window.Product = Product;
