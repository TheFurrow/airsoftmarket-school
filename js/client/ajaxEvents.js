//This function is for newItem.js
//It send ajax event to server with desired data and returns
//callback with redirection path
function ajaxFunction(data, url, type, dataType, pD, cT, callback) {
  //Send the item and wait for result
  $.ajax({
    url:url,
    type:type,
    dataType:dataType,
    data: data,
    processData:pD,
    contentType:cT,
    cache:false,
    success:function(result) {
      //console.log('RESULT',result);
      return callback(result);
    },
    error:function(error) {
      //console.log('ERROR',error.responseJSON.object);
      return callback(error);
    }
  });
}
