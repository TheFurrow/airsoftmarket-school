///////////////
// This file have functionality for animations

//Execute when ready
$(document).ready(function() {

  //Image click (deletion)
  //Activate this
  function imgOnClick(self) {
    if($(self).css(["opacity"]).opacity >= "0.6") {
      $(self).css("opacity","0.3");
      $(self).unbind('mouseenter',hoverOn)
      $(self).unbind('mouseleave',hoverLeave)
      console.log("HOVER OFF");
    }
    else {
      $(self).css("opacity","1");
      $(self).bind('mouseenter',hoverOn)
      $(self).bind('mouseleave',hoverLeave)
    }
  };


  //Image hover
    function hoverOn(self) {
        console.log('hoverOn');
      if($(self).css(["opacity"]).opacity != "0.3") {
        console.log('HOVERING',self);
        //Set opacity smaller
        $(self).animate({
          opacity:"0.6"
        },0);
      }
    }


    function hoverLeave(self) {
      console.log('hoverLeave');
      console.log($(self).css(["opacity"]));
      //Check wether we have clicked image and set image property inactive
      if($(self).css(["opacity"]).opacity != "0.3") {
        console.log('HOVERING OUT',$(self).css(["opacity"]));
        //Set opacity to normal
        $(self).animate({
          opacity:"1"
        },0);
      }
      else {
        console.log("OPACITY ==",$(self).css(["opacity"]))
      }
    }

  ///////
  // Hover
  $('.img-prev-del').bind({
    mouseenter: function(e) {
      hoverOn(this);
      console.log('hover');
    },
    mouseleave: function(e) {
      console.log('hover');
      hoverLeave(this);
    },
    click: function(e) {
      console.log('click');
      imgOnClick(this);
    }
  });





});
