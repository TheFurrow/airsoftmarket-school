/*
<div class="form-group">
  <label for="selectCategory" class="col-xs-3 control-label"><%- item_categoryName %></label>
  <div class="col-xs-9" id="selectCategory">
    <select id="select_dealCategory" class="form-control">
      <option value="value"><%- item_subcategory_weapons %></option>
      <option value="value"><%- item_subcategory_gear %></option>
      <option value="value"><%- item_subcategory_parts %></option>
      <option value="value"><%- item_subcategory_misc %></option>
    </select>
  </div>
</div>


*/


function normalItem_noRemoveBtn() {
  itemIndex = 0;
  var x = '<div class="row itemsRow" data-product-row="'+itemIndex+'">'+
                                  '<div class="col-centered col-xs-12">'+
                                    '<h4>Tuote '+(itemIndex+1)+'</h4>'+
                                    '<div id="itemsGroup" class="form-group">'+
                                      '<label for="dealItem" class="col-xs-3 control-label">'+'Tuote'+'</label>'+
                                      '<div id="dealItem" class="col-xs-5">'+
                                        '<input id="itemName" data-id="'+itemIndex+'-name" type="text" class="form-control" placeholder="Myytävä tuote">'+
                                      '</div>'+
                                      '<div id="dealItemPrice" class="col-xs-4">'+
                                        '<div class="input-group">'+
                                          '<span class="input-group-addon">'+'€'+'</span>'+
                                          '<input id="itemPrice" data-id="'+itemIndex+'-price" type="text" class="form-control" placeholder="hinta">'+
                                        '</div>'+
                                      '</div>'+
                                    '</div>'+
                                    '<div id="itemsGroup" class="form-group">'+
                                      '<label for="dealItemDescription" class="col-xs-3 control-label">'+'Tuotekuvaus'+'</label>'+
                                      '<div id="dealItemDescription" class="col-xs-9">'+
                                        '<textarea id="itemDescription" data-id="'+itemIndex+'-desc" class="form-control" placeholder=""></textarea>'+
                                      '</div>'+
                                    '</div>'+
                                  '</div>'+
                                '</div>';
  return x;

}

function exchangeItem() {
  var x = '<div class="row exchangeRow" data-exchange-row="'+tradeItemIndex+'">'+
                        '  <h4>Oma vaihdokki</h4>'+
                          '  <div class="col-xs-12 col-sm-12">'+

                            '  <div class="form-group">'+
                            '    <label for="dealItem" class="col-xs-3 control-label">Tuote</label>'+
                              '  <div  class="col-xs-9">'+
                              '    <input data-id="'+tradeItemIndex+'-name" id="dealItem" type="text" class="form-control">'+
                              '  </div>'+

                            '  </div>'+
                            '  <div class="form-group">'+

                            '    <label for="dealItem" class="col-xs-3 control-label">Seloste</label>'+
                            '    <div  class="col-xs-9">'+
                            '      <textarea data-id="'+tradeItemIndex+'-desc" id="dealItem" type="text" class="form-control"></textarea>'+
                            '    </div>'+

                            '  </div>'+
                          '  </div>'+
                          '</div>';
  tradeItemIndex++;
  var x2 =                     '<div class="row exchangeRow" data-exchange-row="'+tradeItemIndex+'">'+
                              '  <div class="col-xs-12 col-sm-12">'+
                                ' <h4>Vaihtotuote 1</h4>'+
                                  '<div class="form-group">'+
                                  '  <label for="dealItem" class="col-xs-3 control-label">Tuote</label>'+
                                    '<div  class="col-xs-9">'+
                                    '  <input data-id="'+tradeItemIndex+'-name" id="dealItem" type="text" class="form-control">'+
                                  '  </div>'+

                                  '</div>'+
                                  '<div class="form-group">'+

                                    '<label for="dealItem" class="col-xs-3 control-label">Seloste</label>'+
                                    '<div  class="col-xs-9">'+
                                    '  <textarea data-id="'+tradeItemIndex+'-desc" id="dealItem" type="text" class="form-control"></textarea>'+
                                    '</div>'+

                                  '</div>'+
                                '</div>'+
                          '</div>';
  return x += x2;
}

////////////variables
var itemIndex = 0,
    tradeItemIndex = 0;

//We model a enumeration with object and state below
var NewItem = {
  Exchange : 0,
  Common : 1
}
var newItemState = NewItem.Common;


//This is activated at first when page is loading
$(window).on('load',function() {
  $('#itemContainer div').append('<div class="col-centered col-xs-12 col-sm-12">'+
                                  '<div class="alert alert-info" role="alert">'+
                                    '<p class="text-center"><span><b>Valitse ilmoituksen tyyppi</b> lisätäksesi tuotteita</span></p>'+
                                    '</div>'+
                                  '</div>');
});

//This will be activated when on load
$(document).ready(function() {

  var product;

  $(document).on('click', 'input[id="dealRadio1"]', function() {
    product = new Product($('input[id="dealRadio1"]:checked').val());
  });

  //Listen document events

  //Add new item
  $(document).on('click', '#btn_newItem', function() {
    product.newItem();
  });

  $(document).on('click', '#btn_removeItem', function() {
    switch(newItemState) {

      case NewItem.Common: {
        console.log('removing common item');
        $('div[data-product-row="'+$(this).attr('data-id')+'"]').remove();
        itemIndex--;
        break;
      }
      case NewItem.Exchange: {
        console.log('removing exchange item');
        $('div[data-exchange-row="'+$(this).attr('data-id')+'"]').remove();
        tradeItemIndex--;
        break;
      }
    }
  });

  //Submit item
  $(document).on('click','#submitItem',function() {

    var images = ImageArray;
    console.log('IMAGES',images);

    var formData = new FormData();
    $.each(images,function(index) {
      formData.append('files',images[index]);
    });


    switch(newItemState) {
      case NewItem.Exchange: {
        var arr_items = [];

        $('.exchangeRow').each(function(index) {
          console.log(index);
          var tmpItem = {
            sort: index,
            product: $('input[data-id="'+index+'-name"]').val(),
            description: newLinesToBr($('textarea[data-id="'+index+'-desc"]').val()),
          };

          arr_items.push(tmpItem);
        });

        console.log(arr_items);

        //Create object of item, we will send it to server by ajax
        var object = {
          categories: {
            type: $('input[id="dealRadio1"]:checked', '#form_newDeal').val(),
            category: $('#select_dealCategory').val(),
            subcategory: $('#select_dealSubCategory').val()
          },
          title: $('#title').val(),
          commonDesc: newLinesToBr($('#commonDesc').val()),
          date: Date(),
          items: arr_items
        };

        console.log(object);


          formData.append('object',JSON.stringify(object));

        console.log(formData);

        //Here we will pass our object to ajaxEvent handler file which returns
        //us redirection path.

        ajaxFunction(formData,'/user/new/item/submit', 'POST','',false,false, function(redirectPath) {
          console.log(redirectPath.redirect);
          if(redirectPath.redirect != undefined) {
            window.location = redirectPath.redirect;
          }
          else $('body').append('<p>'+redirectPath.responseJSON.errorMessage+'</p>');
        });
        break;
      }
      case NewItem.Common: {
        var arr_items = [];

        //Put all items in objet and push in arr_items
        $('.itemsRow').each(function(index) {

          //Put item in object and push in temporary array
          var tmpItem = {
            sort: index,
            product: $('input[data-id="'+index+'-name"]').val(),
            description: newLinesToBr($('textarea[data-id="'+index+'-desc"]').val()),
            price: $('input[data-id="'+index+'-price"]').val()
          };

          arr_items.push(tmpItem);
        });

        console.log($('input[name="picture"]')[0].files.length);

        //Create object of item, we will send it to server by ajax
        var object = {
          categories: {
            type: $('input[id="dealRadio1"]:checked', '#form_newDeal').val(),
            category: $('#select_dealCategory').val(),
            subcategory: $('#select_dealSubCategory').val()
          },
          title: $('#title').val(),
          commonDesc: newLinesToBr($('#commonDesc').val()),
          date: Date(),
          items: arr_items
        };

        console.log(object);


          formData.append('object',JSON.stringify(object));

        console.log(formData);

        //Here we will pass our object to ajaxEvent handler file which returns
        //us redirection path.

      ajaxFunction(formData,'/user/new/item/submit', 'POST','',false,false, function(redirectPath) {
          console.log(redirectPath.redirect);
          if(redirectPath.redirect != undefined) {
            window.location = redirectPath.redirect;
          }
          else $('body').append('<p>'+redirectPath.responseJSON.errorMessage+'</p>');
        });
        break;
      }
    }

  });

  $(document).on('change', '#select_dealCategory', function() {
    //console.log($(this).val());

    product.newSubCategory($(this).val())
  });

  //We use modelled enumeration for switch cases
  $(document).on('click', 'input[id="dealRadio1"]:checked', function() {
    if($(this).val() == 'trade') {
      newItemState = NewItem.Exchange;
      //Empty content of .itemContainers child div and append exchange material
      $('#itemContainer div').empty();
      $('#itemContainer div').append(exchangeItem());
    }
    //If we are switching back to some else category, we print out the basic
    else {
      newItemState = NewItem.Common;
      $('#itemContainer div').empty();
      $('#itemContainer div').append(normalItem_noRemoveBtn());
    }

    //Set select to usable
    if($('#select_dealCategory').attr('disabled') && $('input[id="dealRadio1"]:checked')) {
      $('#select_dealCategory').removeAttr('disabled');
    }
    else $(this).attr('disabled');

  })
});


/* FOR NEW ITEM BUTTON



*/
