
//Format date
function setDateFormat(itemDate) {

  var today = new Date();
  date = today.getDate() +'.'+ (today.getMonth() + 1) +'.'+ today.getFullYear();

  var yesterday = today.getDate() - 1
  if(itemDate.getDate() == yesterday && itemDate.getMonth() == today.getMonth()) {
    return 'Eilen';
  }
  else if(itemDate.getDate() == today.getDate() && itemDate.getMonth() == today.getMonth()) {
    return "Tänään";
  }
  else {
    var itemDate = itemDate.getDate() +'.'+ (itemDate.getMonth() + 1) +'.'+ itemDate.getFullYear();
    return itemDate;
  }
}

//Format Type, category and subcategories
function getTypeTitle(rawTitle) {

  switch(rawTitle) {
    case 'sell': {
      return "Myydään";
      break;
    }
    case 'buy': {
      return "Ostetaan";
      break;
    }
    case 'trade': {
      return "Vaihdetaan";
      break;
    }
  }
}

function getCategoryTitle(rawTitle) {
  switch(rawTitle) {
    case 'weapons': {
      return "Aseet";
      break;
    }
    case 'gear': {
      return "Varusteet";
      break;
    }
    case 'parts': {
      return "Osat";
      break;
    }
    case 'misc': {
      return "Sekalainen";
      break;
    }

  }
}

function getSubcategoryTitle(rawTitle) {
  switch(rawTitle) {
    case 'aeg': {
      return ", sähkötoimiset";
      break;
    }
    case 'gas': {
      return ", kaasutoimiset";
      break;
    }
    case 'spring': {
      return ", jousitoimiset";
      break;
    }
    case 'hpa': {
      return ", paineilmatoimiset (HPA)";
      break;
    }
    case 'projects': {
      return ", projektit ja raadot";
      break;
    }
    case 'combat-gear': {
      return ", taisteluvarusteet";
      break;
    }
    case 'clothes': {
      return ", vaatetus";
      break;
    }
    case 'protective': {
      return ", suojavarusteet";
      break;
    }
    case 'tuning': {
      return ", viritysosat";
      break;
    }
    case 'body-parts': {
      return ", aseiden ulko-osat";
      break;
    }
    case 'mags': {
      return ", lippaat";
      break;
    }
    case 'misc': {
      return ", sekalainen";
      break;
    }
    case undefined: {
      return '';
      break;
    }
  }
}
