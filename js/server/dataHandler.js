var dbSchemas = require('./db_schemas.js');

exports.return_UpdatedItemArray = function(item, callback) {
  console.log(item);
  console.log('HANDLER, 6',item)
  try {
    //Create new array
    var saveArray = [];
    console.log('HANDLER 10');

console.log('HANDLER 13');
      //Create and reset required variables
      var items = [];
      var ItemsArray = [];
      var totalPrice = 0;
console.log('HANDLER 18');

      for(var j = 0; j < item.items.length;j++) {
console.log('HANDLER 20');

        //First of all we want to create items objects and push them
        //in pendingItem array
        var tmpItems = {
          sort: item.items[j].sort,
          product: item.items[j].product,
          description:item.items[j].description,
        }

        //Not all items have price
        if(item.items[j].price) {
          tmpItems.price = item.items[j].price;

          //Save total price of items for later use
          totalPrice += parseInt(item.items[j].price);
        }

        //Push items in arrays and move on
        items.push(tmpItems);

        ItemsArray.push(items);

      }

      //Uppercase first letter of title
      item.title = item.title.charAt(0).toUpperCase() + item.title.slice(1);

      //Create new item
      var newItems = new dbSchemas.Item({
        _id:item.itemId,
        categories: {
          type: item.categories.type,
          category: item.categories.category,
          subcategory:item.categories.subcategory
        },
        title: item.title,
        commonDesc:item.commonDesc,
        date: item.date,
        items: ItemsArray[0]
      });

      if(item.items[j].price) {
        newItems.price = totalPrice;
      }

      saveArray.push(newItems);

    console.log('HANLDER,56',saveArray)
    return callback(true,newItems);
  }
  catch(error) {
    return callback(false,error.message);
  }
};

//This function returns array that is created from item(s)
exports.return_PendingItemArray = function(item, user, callback) {
  console.log('WEE',item);
  console.log('WEE',user);
  try{
    //Create new array
    var saveArray = [];

      //Create and reset required variables
      var items = [];
      var pendingItem = [];
      var totalPrice = 0;

      for(var j = 0; j < item.items.length;j++) {

        //First of all we want to create items objects and push them
        //in pendingItem array
        var tmpItems = {
          sort: item.items[j].sort,
          product: item.items[j].product,
          description: item.items[j].description,
        }

        //Not all items have price
        if(item.items[j].price) {
          tmpItems.price = item.items[j].price;

          //Save total price of items for later use
          totalPrice += parseInt(item.items[j].price);
        }

        //Push items in arrays and move on
        items.push(tmpItems);

        pendingItem.push(items);
      }

      //Uppercase first letter of title
      item.title = item.title.charAt(0).toUpperCase() + item.title.slice(1);

      var testArray = [];
      var tmpCategories = {
        type:item.categories.type,
        category:item.categories.category,
        subcategory: item.categories.subcategory
      }
      testArray.push(tmpCategories)
      console.log('111',testArray);

      //Create new item
      var newItems = new dbSchemas.Item({
        itemTag: item.itemTag,
        //userId: user._id,
        categories: testArray[0],
        title: item.title,
        commonDesc: item.commonDesc,
        date: item.date,
        items: pendingItem[0]
      });

      if(item.items[0].price) {
        newItems.price = totalPrice;
      }


    console.log('HANDLER 121',newItems);
    return callback(true,newItems);
  }
  catch(error) {
    return callback(false,error.message);
  }
};

exports.return_ApprovedItemArray = function(items, callback) {
  try {
    var approves_Array = [];
    console.log('OMF',items.approves.length);


    //Changes APPROVER!
    for(var i = 0; i < items.approves.length;i++) {
      var object = {
        _id: items.approves[i].id,
        dateApproved:Date(),
        approver:'Tuisku Saarelainen'
      }

      approves_Array.push(object);
    }
      return callback(approves_Array, true);

  }
  catch(err) {
    console.log('ERROR OCCURED',err.message);
    return callback(false,err.message);
  }
};
