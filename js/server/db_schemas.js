var db_queries = require('./db_query.js');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
exports.Schema = Schema;


//Item schema
var itemSchema = new Schema({
  itemTag: Schema.Types.Mixed,
  userId: Schema.Types.Mixed,
  categories:{
    type: Schema.Types.Mixed,
	  category: Schema.Types.Mixed,
	  subcategory: Schema.Types.Mixed
  },
  title: String,
	commonDesc: Schema.Types.Mixed,
  price: Number,
  date: Date,
  items: [
    {
      sort: Number,
      sold: Boolean,
      product: String,
      description: String,
      price: Number
    }
  ],
  images:[
    {
      itemId: Schema.Types.Mixed,
      images:[{
        name: Schema.Types.Mixed,
        size: Number
      }]

    }
  ]
});
//Export item so it can be used on other components
var Item = mongoose.model('item', itemSchema);
exports.Item = Item;

var imageSchema = new Schema({
  itemId: Schema.Types.Mixed,
  images: [
    {
      name: Schema.Types.Mixed,
      size: Number
    }
  ]
});
var Images = mongoose.model('images', imageSchema);
exports.Images = Images;

//For items that needs to be approved
var pendingItemSchema = new Schema({
  dateAdded: Date
});
var PendingItem = mongoose.model('pendings', pendingItemSchema);
exports.PendingItem = PendingItem;
