var crypto = require('crypto'),
    algorithm = 'aes-128-ctr',
    password = '';

var chars = ['abcdefghijklmnopqrstuyzxäö1234567890ABCDEFGHIJKLMNOPQRSTUYZXÄÖ'];

//We actually change password each time we call this,
//because we want to change the name of file to avoid
//multinames
exports.encrypt = function(value) {
  for(var i = 0; i < 5;i++) {
    password += chars[0][Math.random(62)];
  };
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(value,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
