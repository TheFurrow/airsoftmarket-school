var mongoose = require('mongoose');
var handler = require('./dataHandler.js');
var schemas = require('./db_schemas.js'),
    ObjectId = schemas.Schema.ObjectId;
var cfg = require('../configure.js').dbConfigures;
module.exports.mongoose = mongoose;


////////////////////////////
//Connect into db
mongoose.connect('mongodb://'+cfg.host+'/'+cfg.database+'');
mongoose.Promise = global.Promise;
var db = mongoose.connection;

//Handle connection
db.on('error', console.log.bind('Couldnt connect into db'));
db.once('open', function() {
  console.log("Connected into MongoDB/airsoftmarket");
});

//////////////////////////////////////////////////////////////
///////////////////////////QUERIES////////////////////////////
//////////////////////////////////////////////////////////////


//////
//NEW
exports.new_Images = function(images, item, callback) {

  console.log('OMGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG',item);
  var array = [];
  var imageArray = [];
  for(var i = 0; i < images.length;i++) {
    var tmp = {
      name: images[i].filename,
      size: images[i].size
    }
    imageArray.push(tmp);
  }

  var object = schemas.Images({
    itemId: item._id,
    images: imageArray
  })

  array.push(object);
  console.log('64',array);

  schemas.Images.findOne({itemId:item._id}, function(err,result) {
    if(result) {
      console.log(result.images.length);
      if(result.images.length < 7) {
        console.log('PATH 129');
        schemas.Images.findOneAndUpdate({itemId: item._id}, {$push:{"images": {$each:imageArray}}}, {upsert:true},function(err,result) {
          if(result) {
            console.log(result);
            return callback(true,result)
          }
          else return callback(false,result);
        })
      }
    }
    else {
      console.log('PATH 139');
      //Insert
      object.save(array, function(err,result) {
        if(err) return callback(false,err);
        else return callback(true,result);
      })
    }
  })


}

exports.new_Items = function(msg, callback) {
  console.log('QUERIES 157');
  msg.save(function(err,result) {
    if(err) {
      console.log('NOT THIS 160',err)
      return callback(false, err)
    }
    else {
      console.log('THIS 164',result)
      return callback(true, result)
    }
  })
};

exports.new_PendingItem = function(item, callback) {
  schemas.PendingItem.insertMany(item,function(err,result) {
    if(err) {
       console.log(err);
       return callback(false);
    }
    else {
      //Send true
      return callback(true);
    }
  });
};
