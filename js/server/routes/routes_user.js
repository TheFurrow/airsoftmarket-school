var express = require('express'),
    router = express.Router();
var conf = require('../config.js');
require('../../../server.js').app;
var dbHandler = require('../db_handler.js');

  var multer = require('multer'),
      storage = multer.diskStorage({
            destination: function(req,file,cb) {
              cb(null,'./files');
            },
            filename: function(req,file,cb) {

              switch(file.mimetype) {
                case 'image/jpeg': {
                  cb(null, conf.encrypter.encrypt(file.originalname) + '.jpg')
                  break;
                }
                case 'image/png': {
                  cb(null, conf.encrypter.encrypt(file.originalname) + '.png')
                  break;
                }
              }
            }
          }),
      upload = multer({storage:storage});


  ///////////////////////////////
  //GET: USER FUNCTIONS

  router.get('/new/item',function(req,res) {

      res.render('new_item', {
                            siteTitle: conf.data.siteTitle,
                            item_title: conf.data.item_title,
                            item_titlePlaceholder: conf.data.item_titlePlaceholder(),
                            item_commonDesc_label:conf.data.item_commonDesc_label,
                            item_commonDesc_Placeholder:conf.data.item_commonDesc_Placeholder,
                            //Deal types
                            item_dealType:conf.data.item_dealType,
                            item_dealType_sell:conf.data.item_dealType_sell,
                            item_dealType_trade:conf.data.item_dealType_trade,
                            item_dealType_buy:conf.data.item_dealType_buy,
                            //Categories
                            item_categoryName:conf.data.item_categoryName,
                            item_category_weapons:conf.data.item_category_weapons,
                            item_category_gear:conf.data.item_category_gear,
                            item_category_parts:conf.data.item_category_parts,
                            item_category_misc:conf.data.item_category_misc,
                            //items
                            item_dealItem_label:conf.data.item_dealItem_label,
                            item_dealItem_Name: conf.data.item_dealItemName(),
                            item_dealItem_newItemButton_label:conf.data.item_dealItem_newItemButton_label,
                            item_dealItem_Descriptionlabel:conf.data.item_dealItem_Descriptionlabel,
                            item_dealItem_priceTag: conf.data.item_dealItem_priceTag
                          });

  });


  router.post('/new/item/submit', upload.array('files',7), function(req,res) {

      console.log(req.body.object);
      var object = JSON.parse(req.body.object);
      //Save item in database and send redirection path with nesseccary info
      conf.dbHandler.Save_NewItem(object, req.user, req.files, function(status, msg) {
        if(status == true) {
          res.status(201).send({redirect:'/user/new/item/success'});
        }
        else {
          res.status(201).send({redirect:'/user/new/item/failure',errorMessage:msg});
        }
      });


  });


module.exports = router;
