var express = require('express'),
    router = express.Router();
var conf = require('../config.js');

  //COMMON MESSAGES
  router.get('/common-error',function(req,res) {
    res.render('messages/common_message_form.ejs',
    {
      siteTitle: conf.data.siteTitle,
      user:req.user,
      messageStatus:false,
      messageHeading: conf.data.msg_onError_Heading,
      messageDesc: conf.data.msg_onError_Desc,
      messageButtonUrl:'/',
      messageButtonTitle:'Palaa etusivulle'
    })
  })

  router.get('/login-required-error',function(req,res) {
    res.render('messages/common_message_form.ejs',
    {
      siteTitle: conf.data.siteTitle,
      user:req.user,
      messageStatus:false,
      messageHeading: 'Kirjaudu',
      messageDesc: 'Toimintosi vaatii kirjautumisen, kirjaudu sisään tunnuksillasi!',
      messageButtonUrl:'/',
      messageButtonTitle:'Palaa etusivulle'
    })
  })

  router.get('/user/update-profile/failure',function(req,res) {
    res.render('messages/common_message_form.ejs',
    {
      siteTitle: conf.data.siteTitle,
      user:req.user,
      messageStatus:false,
      messageHeading: 'Vohveli!',
      messageDesc: 'Jokin meni pieleen päivitettäessä tietojasi. Olemme keränneet ongelman tiedot ja välittäneet ne puolestasi kehittäjälle.',
      messageButtonUrl:'/user',
      messageButtonTitle:'Yritä uudelleen'
    })
  })

  router.get('/user/update-profile/success',function(req,res) {
    res.render('messages/common_message_form.ejs',
    {
      siteTitle: conf.data.siteTitle,
      user:req.user,
      messageStatus:true,
      messageHeading: 'Kiitos!',
      messageDesc: 'Profiilitietosi on nyt päivitetty',
      messageButtonUrl:'/user',
      messageButtonTitle:'Palaa profiiliin'
    })
  })

  router.get('/user/missing-contact-error',function(req,res) {
    res.render('messages/common_message_form.ejs',
    {
      siteTitle: conf.data.siteTitle,
      user:req.user,
      messageStatus:false,
      messageHeading: 'Päivitä tietosi',
      messageDesc: 'Jatkaaksesi toimintojasi, on syytä päivittää profiilitietosi. '+
      'Syynä voi olla vaadittu sähköposti ja tai puhelinnumero.',
      messageButtonUrl:'/user',
      messageButtonTitle:'Päivitä tietosi'
    })
  })


  //New Item success
  router.get('/new/item/success',function(req,res) {
        res.render('messages/common_message_form',
        {
          siteTitle: conf.data.siteTitle,
          user:req.user,
          messageStatus:true,
          messageHeading: conf.data.msg_onSuccess_Heading,
          messageDesc: conf.data.msg_itemPending,
          messageButtonUrl:'/',
          messageButtonTitle:'Palaa etusivulle'
        })
  });

  //New item failure
  router.get('/new/item/failure',function(req,res) {
        res.render('messages/common_message_form',
        {
          siteTitle: conf.data.siteTitle,
          user:req.user,
          messageStatus:false,
          messageHeading: conf.data.msg_onError_Heading,
          messageDesc: conf.data.msg_itemFailure,
          messageButtonUrl:'/user/new/item',
          messageButtonTitle:'Yritä uudelleen'
        })
  });

  ////////////////
  //Item removal
  router.get('/user/item/delete/success',function(req,res) {
        res.render('messages/common_message_form',
        {
          siteTitle: conf.data.siteTitle,
          user:req.user,
          messageStatus:true,
          messageHeading: conf.data.msg_onSuccess_Heading,
          messageDesc: conf.data.msg_itemRemovalSuccess_Desc,
          messageButtonUrl:'/user/items',
          messageButtonTitle:'Palaa profiiliin'
        })
  });

  //Item removal failure
  router.get('/user/item/delete/failure',function(req,res) {
        res.render('messages/common_message_form',
        {
          siteTitle: conf.data.siteTitle,
          user:req.user,
          messageStatus:false,
          messageHeading: conf.data.msg_onError_Heading,
          messageDesc: conf.data.msg_onError_Desc,
          messageButtonUrl:'/user/items',
          messageButtonTitle:'Yritä uudelleen'
        })
  });

  //////
  //Modifies

  //Item modify statuses
  //Success
  router.get('/user/item/modify/success', function(req,res) {
    res.render('messages/common_message_form',{
      siteTitle: conf.data.siteTitle,
      user:req.user,
      messageStatus:true,
      messageHeading: 'Kiitos!',
      messageDesc: 'Ilmoituksesi on muokattu ja lähetetty uudelleentarkistettavaksi!',
      messageButtonUrl:'/user/items',
      messageButtonTitle:'Palaa profiiliin'
    })
  });

  //Failure
  router.get('/user/item/modify/failure', function(req,res) {
    res.render('messages/common_message_form',{
      siteTitle: conf.data.siteTitle,
      user:req.user,
      messageStatus:false,
      messageHeading: 'Hö!',
      messageDesc: 'Jokin meni pieleen muokattaessa ilmoitustasi...',
      messageButtonUrl:'/user/items',
      messageButtonTitle:'Yritä uudelleen'
    })
  });

  module.exports = router;
