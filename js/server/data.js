//rihj56i9h459jh45h = Tuisku
//450itbm450tbi45b = RandomGuy
var tmpUserObject = {
  userId:'rihj56i9h459jh45h',
  admin:false
}


////////////////////////////////
/////////// VARIABLES //////////
//////////// CLIENT ////////////

//Random titles for new item
var placeholder = ["Jouleja sylkevä M249 SAW",
                  "Verikauha",
                  "Käytettyjä kuulia",
                  "Jenkki/Venäjä varusteita",
                  "M05 Lippis",
                  "Aloittelijalle paketti",
                  "HPA-koneisto ja tavarat",
                  "Juomarakon reppu, MIL-TEC",
                  "Toimiva AK-102",
                  "DIY Ghillie -tavaraa"
                ];
module.exports.placeholder = placeholder;
var randomDate = ["Tänään",
                  "Eilen",
                  "19.9.2016",
                  "19.9.2016",
                  "18.9.2016",
                  "17.9.2016",
                  "17.9.2016",
                  "16.9.2016",
                  "15.9.2016",
                  "15.9.2016",
                  "15.9.2016",
                  "14.9.2016"
                ];
module.exports.randomDate = randomDate;

//Get these from database, later
var siteTitle = "Airsoft kirpputori";
module.exports.siteTitle = siteTitle;

var msg_onSuccess_Heading = "Kiitos!",
    msg_onError_Heading = "Ohhoh!",
    msg_onError_Desc = "Toiminnossasi ilmeni virhe ja se tutkitaan piakkoin. Yritä uudelleen!",
    msg_itemPending = "Ilmoituksesi tarkistetaan ensin.",
    msg_itemFailure = "Jokin meni pieleen :(",
    msg_itemRemovalSuccess_Desc = 'Ilmoituksesi on poistettu.'
module.exports.msg_onSuccess_Heading = msg_onSuccess_Heading;
module.exports.msg_onError_Heading = msg_onError_Heading;
module.exports.msg_onError_Desc = msg_onError_Desc;
module.exports.msg_itemPending = msg_itemPending;
module.exports.msg_itemFailure = msg_itemFailure;
module.exports.msg_itemRemovalSuccess_Desc = msg_itemRemovalSuccess_Desc;


var table_name = "Nimi",
    table_delete = "Poista?",
    table_approve = "Hyväksy";
module.exports.table_name = table_name;
module.exports.table_delete = table_delete;
module.exports.table_approve = table_approve;

var item_title = "Otsikko",
    item_label = "Ilmoitus",
    item_totalPrice = "Yhteensä";
module.exports.item_title = item_title;
module.exports.item_label = item_label;
module.exports.item_totalPrice = item_totalPrice;
var view_item_MyItem_label = "Vaihdettava tuote",
    view_item_ExchangeItem_label = "Nämä kiinnostavat vaihdossa";
module.exports.item_totalPrice = item_totalPrice;
module.exports.item_totalPrice = item_totalPrice;
var item_dealType = "Ilmoituksen tyyppi",
    item_dealType_sell = "Myy",
    item_dealType_sell_Title = "Myydään",
    item_dealType_trade = "Vaihda",
    item_dealType_trade_Title = "Vaihtoon",
    item_dealType_buy = "Osta",
    item_dealType_buy_Title = "Ostetaan",
    //Item itself
    item_dealItem_label = "Lisää tuote",
    item_dealItem_priceTag = "€"
    item_dealItem_newItemButton_label = "Lisää uusi tuote",
    item_dealItem_Descriptionlabel = "Tuoteseloste",
    item_dealItem_Name = ["CYMA AK-74M",
                          "CA M249 SAW",
                          "Ritilämaski, begadi",
                          "TM G36C",
                          "6b27-kypärä, oikea",
                          "ACM lamppu, miljoona lumenia",
                          "Kaikki varusteet",
                          "BB King 0.28g kuulia",
                          "Woodland puku, S",
                          "Multicam puku, L",
                        ],
    item_commonDesc_label = "Yleisesti",
    item_commonDesc_Placeholder = "Kerro yleisesti ilmoituksestasi",
module.exports.item_commonDesc_label = item_commonDesc_label,
module.exports.item_commonDesc_Placeholder = item_commonDesc_Placeholder,
module.exports.item_dealType = item_dealType;
module.exports.item_dealType_sell = item_dealType_sell;
module.exports.item_dealType_sell_Title = item_dealType_sell_Title;
module.exports.item_dealType_trade = item_dealType_trade;
module.exports.item_dealType_trade_Title = item_dealType_trade_Title;
module.exports.item_dealType_buy = item_dealType_buy;
module.exports.item_dealType_buy_Title = item_dealType_buy_Title;
module.exports.item_dealItem_label = item_dealItem_label;
module.exports.item_dealItem_priceTag = item_dealItem_priceTag;
module.exports.item_dealItem_newItemButton_label = item_dealItem_newItemButton_label;
module.exports.item_dealItem_Descriptionlabel = item_dealItem_Descriptionlabel;
module.exports.item_dealItem_Name = item_dealItem_Name;


var item_categoryName = "Kategoria",
    //Weapons
    item_category_weapons = "Aseet",
    //Gear
    item_category_gear = "Varusteet",
    //Misc
    item_category_misc = "Sekalainen",
    //Parts
    item_category_parts = "Osat";
module.exports.item_categoryName = item_categoryName;
module.exports.item_category_weapons = item_category_weapons;
module.exports.item_category_gear = item_category_gear;
module.exports.item_category_misc = item_category_misc;
module.exports.item_category_parts = item_category_parts;

var item_subcategory_Name = "Alakategoria",
    item_subcategory_aeg = "Sähköaseet",
    item_subcategory_gas = "Kaasutoimiset",
    item_subcategory_spring = "Jousitoimiset",
    item_subcategory_hpa = "Paineilmatoimiset (HPA)",
    //Gear
    item_subcategory_combatgear = "Taisteluvarusteet",
    item_subcategory_clothes = "Vaatetus",
    item_subcategory_protective = "Suojavarusteet",
    //Parts
    item_subcategory_tuning = "Viritysosat",
    item_subcategory_bodyparts = "Aseiden ulko-osat",
    item_subcategory_mags = "Lippaat",
    //misc
    item_subcategory_misc = "Sekalaista";
module.exports.item_subcategory_Name = item_subcategory_Name;
module.exports.item_subcategory_aeg = item_subcategory_aeg;
module.exports.item_subcategory_gas = item_subcategory_gas;
module.exports.item_subcategory_spring = item_subcategory_spring;
module.exports.item_subcategory_hpa = item_subcategory_hpa;
module.exports.item_subcategory_combatgear = item_subcategory_combatgear;
module.exports.item_subcategory_clothes = item_subcategory_clothes;
module.exports.item_subcategory_tuning = item_subcategory_tuning;
module.exports.item_subcategory_protective = item_subcategory_protective;
module.exports.item_subcategory_bodyparts = item_subcategory_bodyparts;
module.exports.item_subcategory_mags = item_subcategory_mags;
module.exports.item_subcategory_misc = item_subcategory_misc;

function item_titlePlaceholder() {
  return placeholder[Math.round(Math.random() * 10)];
}
function item_dealItemName() {
  return item_dealItem_Name[Math.round(Math.random() * 10)];
}
module.exports.item_titlePlaceholder = item_titlePlaceholder;
module.exports.item_dealItemName = item_dealItemName;
